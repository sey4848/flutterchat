import 'package:chat/component/my_chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ChatController extends GetxController {
  late FocusNode focusNode;
  RxList<Widget> chats = <Widget>[].obs;
  var textController = TextEditingController();

  @override
  void onInit() {
    focusNode = FocusNode();
    super.onInit();
  }

  void sandMessage(text) {
    textController.clear();

    chats.add(
      MyChat(
        text: text,
        time: DateFormat("a K:m")
            .format(DateTime.now())
            .replaceAll("AM", "오전")
            .replaceAll("PM", "오후"),
      )
    );
    focusNode.requestFocus();
  }

  void receiveMessage() {

  }
}


